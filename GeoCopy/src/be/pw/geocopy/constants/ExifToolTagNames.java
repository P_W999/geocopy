/*******************************************************************************
 * Copyright 2012 PW_999
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package be.pw.geocopy.constants;

/**
 * Some constants.
 * @author phillip
 */
public final class ExifToolTagNames {

	/**
	 * Hidden constructor.
	 */
	private ExifToolTagNames() {
	}

	public static final String GPS_LATITUDE_REF = "-GPSLatitudeRef";
	public static final String GPS_LATITUDE = "-GPSLatitude";
	public static final String GPS_LONGITUDE_REF = "-GPSLongitudeRef";
	public static final String GPS_LONGITUDE = "-GPSLongitude";
	public static final String GPS_ALTITUDE_REF = "-GPSAltitudeRef";
	public static final String GPS_ALTITUDE = "-GPSAltitude";

	public static final String TAGS_FROM_FILE = "-tagsFromFile";

}
