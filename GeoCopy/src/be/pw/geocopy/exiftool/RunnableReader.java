/*******************************************************************************
 * Copyright 2012 PW_999
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package be.pw.geocopy.exiftool;

import java.io.BufferedReader;
import java.io.IOException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * A Runnable implementation which can read, independently from the rest of the application, the info which is returned by a BufferedReader.
 * @author phillip
 */
class RunnableReader implements Runnable {

	private static final Log LOG = LogFactory.getLog(RunnableReader.class);
	private boolean error = false;

	private BufferedReader reader;

	/**
	 * Creates a new instance of the reader runnable.
	 * @param reader the read which shall be read
	 * @param error whether the reader is going to spit out error messages (thus indicating that we must log it as an error)
	 */
	public RunnableReader(final BufferedReader reader, final boolean error) {
		this.reader = reader;
		this.error = error;
	}

	/**
	 * {@inheritDoc}
	 */
	@Override
	public void run() {
		try {
			String line;
			while ((line = reader.readLine()) != null) {
				if (error) {
					LOG.error(line);
				} else {
					LOG.info(line);
				}
			}
			LOG.trace("Reader thread finished");
		} catch (IOException e) {
			LOG.error("Error in reader thread", e);
		} finally {
			try {
				reader.close();
				LOG.trace("Reader closed");
			} catch (IOException e) {
				LOG.error("Failed to close reader", e);
			}
		}
	}

}
