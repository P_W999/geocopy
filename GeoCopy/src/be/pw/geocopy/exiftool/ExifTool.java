/*******************************************************************************
 * Copyright 2012 PW_999
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package be.pw.geocopy.exiftool;

import static be.pw.geocopy.constants.ExifToolTagNames.GPS_ALTITUDE;
import static be.pw.geocopy.constants.ExifToolTagNames.GPS_ALTITUDE_REF;
import static be.pw.geocopy.constants.ExifToolTagNames.GPS_LATITUDE;
import static be.pw.geocopy.constants.ExifToolTagNames.GPS_LATITUDE_REF;
import static be.pw.geocopy.constants.ExifToolTagNames.GPS_LONGITUDE;
import static be.pw.geocopy.constants.ExifToolTagNames.GPS_LONGITUDE_REF;
import static be.pw.geocopy.constants.ExifToolTagNames.TAGS_FROM_FILE;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * This class deals ExifTool.exe .
 * @author phillip
 */
public class ExifTool {

	private static final Log LOG = LogFactory.getLog(ExifTool.class);

	private File argFile;
	private Process p;
	private FileWriter fw;
	private int cnt = 0;

	/**
	 * Create a new instance of ExifTool.<br />
	 * Prepares the arguments file for ExifTool daemon mode. Do note that this does not launch ExifTool.exe, you need the start() method for that.<br />
	 */
	public ExifTool() {
		try {
			argFile = new File(".\\args");
			fw = new FileWriter(argFile);
			fw.write("");
			fw.flush();
		} catch (Exception e) {
			LOG.fatal("Exception while initializing the ExifTool, killing the application", e);
			throw new RuntimeException("Failed to initialize ExifTool");
		}
	}

	/**
	 * Starts the processing of the args file.<br />
	 * For good operation you should have at least called any of the following methods (otherwise nothing happens): <br />
	 * @see be.pw.geocopy.exiftool.ExifTool.add(File, File)
	 * @see be.pw.geocopy.exiftool.ExifTool.recover(File)
	 * @see be.pw.geocopy.exiftool.ExifTool.clean(File)
	 */
	public final void start() {
		try {
			appendLine("-stay_open");
			appendLine("false");
			fw.flush();
			Thread exifThread = new Thread(new Runnable() {

				@Override
				public void run() {
					try {
						p = Runtime.getRuntime().exec(System.getProperty("exiftool.path") + " -stay_open True -@ \"" + argFile.getCanonicalPath() +"\"");
						BufferedReader bri = new BufferedReader(new InputStreamReader(p.getInputStream()));
						BufferedReader bre = new BufferedReader(new InputStreamReader(p.getErrorStream()));
						Thread inputReaderThread = new Thread(new RunnableReader(bri, false));
						inputReaderThread.setName("inputReaderThread");
						inputReaderThread.start();
						Thread errorReaderThread = new Thread(new RunnableReader(bre, true));
						errorReaderThread.setName("errorReaderThread");
						errorReaderThread.start();
						p.waitFor();
						p.destroy();
					} catch (Exception e) {
						LOG.error("Exception occured", e);
					}
				}
			});
			exifThread.setName("exifThread");
			exifThread.start();
			exifThread.join(120000);
			try {
				p.exitValue();
				LOG.trace("ExifTool finished gracefully");
			} catch (Exception e) {
				LOG.error("ExifTool was still running, killing it now");
				p.destroy();
			}

			LOG.info("Exiftool has finished the processing");
		} catch (Exception e) {
			LOG.error("Exception occured", e);
		}
	}

	/**
	 * Adds statements for copying the GPS-info from the 'from file' to the 'to file' to the args file.
	 * @param from the file which contains the GPS info.
	 * @param to the file to which the GPS info must be copied.
	 * @throws IOException if any
	 */
	public final void add(final File from, final File to) throws IOException {
		LOG.trace("Adding file copy task from=" + from + ", to=" + to + " with counter at " + cnt);
		appendLine(TAGS_FROM_FILE);
		appendLine(from.getCanonicalPath());
		appendLine(GPS_ALTITUDE);
		appendLine(GPS_ALTITUDE_REF);
		appendLine(GPS_LATITUDE);
		appendLine(GPS_LATITUDE_REF);
		appendLine(GPS_LONGITUDE);
		appendLine(GPS_LONGITUDE_REF);
		appendLine(to.getCanonicalPath());
		if (LOG.isTraceEnabled()) {
			appendLine("-echo");
			appendLine(from.getCanonicalPath());
			appendLine("-echo");
			appendLine(to.getCanonicalPath());
		} /*else {
			appendLine("-q");
		}*/
		appendLine("-execute" + cnt++);
		fw.flush();
	}

	/**
	 * Adds statements to recover modified files.
	 * @param directory the directory where the files must be recovered.
	 * @throws IOException if any
	 */
	public final void recover(final File directory) throws IOException {
		LOG.trace("Adding recover task");
		appendLine("-restore_original");
		appendLine(directory.getCanonicalPath());
		appendLine("-execute");
		fw.flush();
	}

	/**
	 * Adds the statements to remove the recovery files.
	 * @param directory the directory where the recovery files must be removed.
	 * @throws IOException if any
	 */
	public final void clean(final File directory) throws IOException {
		appendLine("-delete_original!");
		appendLine("-ext");
		appendLine("jpg");
		appendLine(directory.getCanonicalPath());
		appendLine("-execute");
		fw.flush();
	}

	/**
	 * Adds a new file to the args file for the daemon mode.
	 * @param o the argument to add
	 * @throws IOException if any
	 */
	private void appendLine(final String o) throws IOException {
		LOG.trace("[APPENDER]" + o);
		fw.append(o + "\n");
	}

}
