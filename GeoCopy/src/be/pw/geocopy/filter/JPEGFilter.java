/*******************************************************************************
 * Copyright 2012 PW_999
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package be.pw.geocopy.filter;

import java.io.File;
import java.io.FileFilter;
import java.io.IOException;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import com.drew.imaging.ImageMetadataReader;
import com.drew.imaging.ImageProcessingException;
import com.drew.metadata.Directory;
import com.drew.metadata.Metadata;
import com.drew.metadata.exif.GpsDirectory;

/**
 * Filters out JPEG files which contain GPS data in their EXIF tags.
 * @author phillip
 */
public class JPEGFilter implements FileFilter {

	private static final Log LOG = LogFactory.getLog(JPEGFilter.class);

	/**
	 * {@inheritDoc}
	 */
	@Override
	public final boolean accept(final File pathname) {
		try {
			LOG.trace("Examining file " + pathname.getCanonicalPath());
			if (pathname.getName().toLowerCase().endsWith("jpg")) {
				Metadata metadata = ImageMetadataReader.readMetadata(pathname);
				Directory directory = metadata.getDirectory(GpsDirectory.class);
				if (directory != null) {
					if (directory.containsTag(GpsDirectory.TAG_GPS_LATITUDE) && directory.containsTag(GpsDirectory.TAG_GPS_LONGITUDE)) {
						LOG.trace("GPS tags were found for file " + pathname.getName() + " -LAT:" + directory.getString(GpsDirectory.TAG_GPS_LATITUDE) + " - LON:" + directory.getString(GpsDirectory.TAG_GPS_LONGITUDE));
						return true;
					}
				}
				return false;
			}
		} catch (ImageProcessingException e) {
			LOG.warn(e.getMessage());
		} catch (IOException e) {
			LOG.warn(e.getMessage());
		}
		return false;
	}
}
