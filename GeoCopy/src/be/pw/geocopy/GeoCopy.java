/*******************************************************************************
 * Copyright 2012 PW_999
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package be.pw.geocopy;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

import be.pw.geocopy.exiftool.ExifTool;
import be.pw.geocopy.filter.JPEGFilter;
import be.pw.geocopy.logging.PerformanceLogger;

/**
 * Main class to run GeoCopy.
 * @author phillip
 */
public final class GeoCopy {
	/**
	 * Hidden constructor.
	 */
	private GeoCopy() {
	}

	private static final Log LOG = LogFactory.getLog(GeoCopy.class);
	private static File exifToolPath = new File(".\\exiftool.exe");
	private static File fromDirectory = new File(".\\from");
	private static File toDirectory = new File(".\\to");

	/**
	 * The main class, decides which operation is going to be performed.
	 * @param args the arguments, either one of copy, restore, clean
	 */
	public static void main(final String[] args) {
		try {
			LOG.trace("Arguments given: " + Arrays.asList(args));
			if (args == null || args.length == 0) {
				System.out.println("No command given. Possible commands are:");
				System.out.println("- copy");
				System.out.println("- clean");
				System.out.println("- restore");
				System.out.println("You can specify the from and to directories using the -from=path and -to=path arguments");
				System.out.println("- Path must be a relative path and may not end with a \\");
				return;
			} else {
				if (System.getProperty("exiftool.path") == null || System.getProperty("exiftool.path").isEmpty()) {
					LOG.trace("Setting path to exiftool in exiftool.path property to " + exifToolPath.getCanonicalPath());
					System.setProperty("exiftool.path", exifToolPath.getCanonicalPath());
				}
				exifToolPath = new File(System.getProperty("exiftool.path"));
				if (!exifToolPath.exists()) {
					LOG.error("Could not find ExifTool.exe, must be in " + exifToolPath.getCanonicalPath());
					return;
				}
				LOG.info("Exiftool executable should be in " + System.getProperty("exiftool.path"));

				for (int i = 1; i < args.length; ++i) {
					String s= args[i];
					if (s.equals("-from")) {
						++i;
						LOG.trace("User specified from path as " + args[i]);
						fromDirectory = new File(".\\"+ args[i]);
					} else if (s.equals("-to")) {
						++i;
						LOG.trace("User specified to path as " + args[i]);
						toDirectory = new File(".\\"+ args[i]);
					} else {
						LOG.error("Unknown argument: " +s);
						System.out.println("Perhaps your path ended in a \\ ? ");
						return;
					}
				}
				
				if (!fromDirectory.exists()) {
					LOG.error("From directory does not exists or not found, should be located at: " + fromDirectory.getCanonicalPath());
					return;
				}
				if (!fromDirectory.isDirectory()) {
					LOG.error("From directory is not a directory: " + fromDirectory.getCanonicalPath());
					return;
				}

				if (!toDirectory.exists()) {
					LOG.error("To directory does not exists or not found, should be located at: " + toDirectory.getCanonicalPath());
					return;
				}
				if (!toDirectory.isDirectory()) {
					LOG.error("To directory is not a directory: " + toDirectory.getCanonicalPath());
					return;
				}

				if (args[0].equalsIgnoreCase("copy")) {
					PerformanceLogger copyPerf = new PerformanceLogger("Copy process", GeoCopy.class);
					copyPerf.start();
					copy();
					copyPerf.stop();
					return;
				}
				if (args[0].equalsIgnoreCase("restore")) {
					PerformanceLogger restorePerf = new PerformanceLogger("Restore process", GeoCopy.class);
					restorePerf.start();
					restore();
					restorePerf.stop();
					return;
				}
				if (args[0].equals("clean")) {
					PerformanceLogger cleanPerf = new PerformanceLogger("Clean process", GeoCopy.class);
					cleanPerf.start();
					clean();
					cleanPerf.stop();
					return;
				}
			}
		} catch (IOException e) {
			LOG.fatal("Failed to start the application", e);
		}

	}

	/**
	 * This method uses ExifTool to restore the altered files to their original state (if these files are present).
	 */
	private static void restore() {
		try {
			LOG.info("Starting Restore");
			ExifTool tool = new ExifTool();
			tool.recover(toDirectory);
			tool.start();
			LOG.info("Ending Restore");
		} catch (Exception e) {
			LOG.fatal("Exception in main thread", e);
		}
	}

	/**
	 * This method uses ExifTool to clean the back-up files generated by ExifTool.
	 */
	private static void clean() {
		try {
			LOG.info("Starting Clean");
			System.out.println("WARNING ! The cleaning process will remove all back-up files !");
			System.out.println("Are you sure ? [y/n]");
			BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
			String result = br.readLine();
			while (result != null && !result.equalsIgnoreCase("y") && !result.equalsIgnoreCase("n")) {
				System.out.println("Are you sure ? [y/n]");
				result = br.readLine();
			}
			if (result == null || result.equals("n")) {
				return;
			}
			ExifTool tool = new ExifTool();
			tool.clean(toDirectory);
			tool.start();
			LOG.info("Ending Clean");
		} catch (Exception e) {
			LOG.fatal("Exception in main thread", e);
		}
	}

	/**
	 * This method uses ExifTool the copy the Geo information from one directory to another.
	 */
	private static void copy() {
		try {
			LOG.info("Starting Copy");

			ExifTool tool = new ExifTool();
			PerformanceLogger prepPerf = new PerformanceLogger("CopyJob: preparing", GeoCopy.class);
			prepPerf.start();
			File[] fromFiles = fromDirectory.listFiles(new JPEGFilter());
			List<File> toFiles = new ArrayList<File>();
			for (File fromFile : fromFiles) {
				LOG.trace("From file = " + fromFile.getCanonicalPath());
				File toFile = new File(toDirectory.getCanonicalPath(), fromFile.getName());
				LOG.trace("To file = " + toFile.getCanonicalPath());
				if (!toFile.exists()) {
					LOG.warn("To file " + toFile.getName() + " not found, skipping");
				} else {
					toFiles.add(toFile);
					tool.add(fromFile, toFile);
				}
			}
			prepPerf.stop();
			PerformanceLogger copyPerf = new PerformanceLogger("CopyJob: copying", GeoCopy.class);
			copyPerf.start();
			tool.start();
			copyPerf.stop();

			LOG.info("Ending Copy");
		} catch (Exception e) {
			LOG.fatal("Exception in main thread", e);
		}
	}

}
