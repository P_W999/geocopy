/*******************************************************************************
 * Copyright 2012 PW_999
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *   http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 ******************************************************************************/
package be.pw.geocopy.logging;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;

/**
 * Helps logging the performance of certain operations.
 * @author phillip
 */
public class PerformanceLogger {

	private static final Log LOG = LogFactory.getLog(PerformanceLogger.class);
	private static final double MAG = 1000000.0;
	private String operation;
	@SuppressWarnings("rawtypes")
	private Class clazz;
	private long from = 0;
	private long to = 0;
	private boolean enabled = false;

	/**
	 * Creates a new PerformanceLogger for the specified operation. The performance shall be logged as a trace message.
	 * @param operation the operation which will be executed, this is just descriptive
	 * @param clazz the class which is creating this PerformanceLogger
	 */
	@SuppressWarnings("rawtypes")
	public PerformanceLogger(final String operation, final Class clazz) {
		this.operation = operation;
		this.clazz = clazz;
		enabled = LogFactory.getLog(getClass()).isTraceEnabled();
	}

	/**
	 * Starts the measurement.
	 */
	public final void start() {
		this.from = System.nanoTime();
	}

	/**
	 * Stops the measurement and logs the result.
	 */
	public final void stop() {
		this.to = System.nanoTime();
		if (enabled) {
			if ((to - from) < 100) {
				LOG.trace(clazz.getSimpleName() + " - " + operation + " took " + ((to - from) / MAG) + " ms");
			} else {
				LOG.trace(clazz.getSimpleName() + " - " + operation + " took " + ((to - from) / (MAG * 1000)) + " s");
			}
		}
	}

}
